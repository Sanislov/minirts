﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[ExecuteInEditMode]
public class CityController : MonoBehaviour
{
    public Button[] b_units;
    public Text ui_pop;

    public Transform unitPrefab;

    public Transform[] garrison;
    public int population = 30;
    public int maxPopulation = 100;
    public int minPopulation = 20;
    public int owner = 0;
    public int x, y;

    [Header("Parent Grid Node")]
    public GridNode parentGrid;

    private float _t = 0f;


    // Use this for initialization
    void Start()
    {
        garrison = new Transform[3];
        //b_units = new Button[3];
        b_units[0].onClick.AddListener(() => Unit_click(0));
        b_units[1].onClick.AddListener(() => Unit_click(1));
        b_units[2].onClick.AddListener(() => Unit_click(2));
        ui_pop.text = population.ToString();
    }

	// Update is called once per frame
	void Update ()
    {
        _t += Time.deltaTime;

        if (_t >= 1f && population < maxPopulation)
        {
            _t = 0f;
            population++;
            ui_pop.text = population.ToString();                        
        }
    }

    public void Unit_click(int slotID)
    {
        Debug.Log("You have clicked the " + slotID.ToString() + "slot.");
        if (population > minPopulation + 5 && garrison[slotID] == null)
        {
            garrison[slotID] = Instantiate(unitPrefab, transform.position, Quaternion.identity);
            population -= 5;
            b_units[slotID].GetComponent<Image>().color = Color.green;
            ui_pop.text = population.ToString();
        }
    }


}
