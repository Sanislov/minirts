﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEditor;
using UnityEditor.SceneManagement;

[ExecuteInEditMode]
public class MapGrid : MonoBehaviour {

    public GridDataObject MapData;

    public EditorGrid EditorGridScript;
    public GameObject CityPrefab;
    public GameObject RoadPrefab;

    public float stepSize;
    public int gridX, gridY;

    public List<GridNode> path;

    Vector2Int[] griddirs = new Vector2Int[4] {  new Vector2Int(0, 1), new Vector2Int(1, 0), new Vector2Int(0, -1), new Vector2Int(-1, 0) };

    private void Awake()
    {
        stepSize = MapData.stepSize;
        gridX = MapData.gridX;
        gridY = MapData.gridY;
    }

    void OnDrawGizmos()
    {
        foreach (GridNode n in MapData.MapArray)
        {
            Gizmos.color = Color.white;
            if (n.roads.Any(x => x)) { Gizmos.color = Color.green; }
            if (path != null)
                if (path.Contains(n))
                    Gizmos.color = Color.red;

            Gizmos.DrawCube(n.wPos + transform.position, new Vector3(5f,1f,5f));
        }
    }

    // Use this for initialization
    public void BuildMap()
    {
        Debug.Log("buildMap!");
        CreateMap();
        EditorGridScript = GetComponent<EditorGrid>();
        EditorGridScript.GenerateGrid(gridX, gridY, stepSize);
    }

    // Fill up grid with nodes
    public void CreateMap()
    {
        MapData.Cities = new List<GameObject>();
        MapData.MapArray = new GridNode[gridX * gridY];
        MapData.gridX = gridX;
        MapData.gridY = gridY;
        for (int i = 0; i < gridX; i++)
        {
            for (int j = 0; j < gridY; j++)
            {
                //GridNode actGrid = ScriptableObject.CreateInstance<GridNode>();
                GridNode actGrid = new GridNode();
                actGrid.wPos = new Vector3(i * stepSize, 0.0f, j * stepSize);
                actGrid.gPos = new Vector2Int(i, j);

                // GridNode actGrid = new GridNode(i, j, stepSize);

                MapData.MapArray[i * gridX + j] = actGrid;
            }
        }
        EditorUtility.SetDirty(MapData);
        EditorSceneManager.MarkSceneDirty(EditorSceneManager.GetActiveScene());
    }

    // Get gridnode by tri
    public GridNode GetNode(int triID)
    {
        int x = (triID / 2) / gridX;
        int y = (triID / 2) % gridY;
        return MapData.MapArray[x * gridX + y];
    }

    // Get gridnode by pos
    public GridNode GetNodebyPos(Vector2Int pos)
    {
        return MapData.MapArray[pos.y * gridX + pos.y];
    }

    // Get Neighbours
    public List<GridNode> GetNeighbours(GridNode node)
    {
        List<GridNode> neighbours = new List<GridNode>();

        for (int l = 0; l < 4; l++)
        {   
            int checkX = node.gPos.x + griddirs[l].x;
            int checkY = node.gPos.y + griddirs[l].y;

            if (checkX >= 0 && checkX < gridX && checkY >= 0 && checkY < gridY)
            {
                if (MapData.MapArray[checkX * gridX + checkY].roads.Any(e => e))
                { neighbours.Add(MapData.MapArray[checkX * gridX + checkY]); }
            }       
        }
        return neighbours;
    }

    // Create City
    public void CreateCity(int triID)
    {
        int x = (triID / 2) / gridX;
        int y = (triID / 2) % gridY;
        Vector3 pos = new Vector3(x * stepSize, 0.0f , y * stepSize);
        if ((MapData.MapArray[x * gridX + y]).City == null)
        {
            GameObject newCity = Instantiate(CityPrefab, pos, Quaternion.identity, transform);
            CityController newCityC = newCity.GetComponent<CityController>();
            newCityC.parentGrid = MapData.MapArray[x * gridX + y];
            (MapData.MapArray[x * gridX + y]).City = newCity;
            MapData.Cities.Add(newCity);
        }      
    }

    // Delete City
    public void DeleteCity(int triID)
    {
        int x = (triID / 2) / gridX;
        int y = (triID / 2) % gridY;
        MapData.Cities.Remove((MapData.MapArray[x * gridX + y]).City);
        DestroyImmediate((MapData.MapArray[x * gridX + y]).City);
        (MapData.MapArray[x * gridX + y]).City = null;
        EditorSceneManager.MarkSceneDirty(EditorSceneManager.GetActiveScene());
    }

    // create Road
    public void CreateRoad(GridNode last, GridNode act, int sideL, int sideA)
    {
        if (last.roadObjects[sideL] == null)
        {
            switch (sideL)
            {
                case 0:
                    last.roadObjects[sideL] = Instantiate(RoadPrefab, last.wPos, Quaternion.identity, transform);
                    break;
                case 1:
                    last.roadObjects[sideL] = Instantiate(RoadPrefab, last.wPos, Quaternion.AngleAxis(90, Vector3.up), transform);
                    break;
                case 2:
                    last.roadObjects[sideL] = Instantiate(RoadPrefab, last.wPos, Quaternion.AngleAxis(180, Vector3.up), transform);
                    break;
                case 3:
                    last.roadObjects[sideL] = Instantiate(RoadPrefab, last.wPos, Quaternion.AngleAxis(270, Vector3.up), transform);
                    break;
            }
        }

        if (act.roadObjects[sideA] == null)
        {
            switch (sideA)
            {
                case 0:
                    act.roadObjects[sideA] = Instantiate(RoadPrefab, act.wPos, Quaternion.identity, transform);
                    break;
                case 1:
                    act.roadObjects[sideA] = Instantiate(RoadPrefab, act.wPos, Quaternion.AngleAxis(90, Vector3.up), transform);
                    break;
                case 2:
                    act.roadObjects[sideA] = Instantiate(RoadPrefab, act.wPos, Quaternion.AngleAxis(180, Vector3.up), transform);
                    break;
                case 3:
                    act.roadObjects[sideA] = Instantiate(RoadPrefab, act.wPos, Quaternion.AngleAxis(270, Vector3.up), transform);
                    break;
            }
        }
        EditorUtility.SetDirty(MapData);
        EditorSceneManager.MarkSceneDirty(EditorSceneManager.GetActiveScene());
    }

    // delete Road
    public void DeleteRoad(GridNode last, GridNode act, int sideL, int sideA)
    {

        DestroyImmediate(last.roadObjects[sideL]);     
        DestroyImmediate(act.roadObjects[sideA]);
        last.roadObjects[sideL] = null;
        act.roadObjects[sideA] = null;
        EditorUtility.SetDirty(MapData);
        EditorSceneManager.MarkSceneDirty(EditorSceneManager.GetActiveScene());
    }
}
