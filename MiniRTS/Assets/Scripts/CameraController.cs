﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

    public float wasdmul;

	// Use this for initialization
	void Start () {

    }
	
	// Update is called once per frame
	void Update ()
    {
        float xAxisValue = Input.GetAxis("Horizontal") * wasdmul;
        float zAxisValue = Input.GetAxis("Vertical") * wasdmul;

        transform.Translate(new Vector3(xAxisValue, 0.0f, zAxisValue), Space.World);
    }
}
