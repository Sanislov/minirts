﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Create Grid mesh object
[RequireComponent(typeof(MeshFilter), typeof(MeshRenderer) , typeof(MeshCollider) )]
public class EditorGrid : MonoBehaviour {

	public MapGrid MapDataScript;

	private Mesh mesh;
	private Vector3[] vertices;
    private Vector2[] uvs;
    private Color[] colors;
	private int[] triangles;


	public void GenerateGrid(int xSize, int ySize, float stepSize) 
	{	
		MapDataScript = transform.GetComponent<MapGrid>();


		GetComponent<MeshFilter>().mesh = mesh = new Mesh();

		mesh.name = "Editor Grid";

		vertices = new Vector3[xSize * ySize * 6];
        uvs = new Vector2[vertices.Length];
        colors = new Color[vertices.Length];
		triangles = new int[xSize * ySize * 6];
		for (int x = 0, v = 0, f = 0; x < xSize; x++) 
		{
			for (int y = 0; y < ySize; y++, v+=6, f+=6 ) 
			{
                //float height = (x + y) / 10.0f;
                float xu = x * stepSize;
                float yu = y * stepSize;
                // vertices
				vertices[v+0] = new Vector3(xu - stepSize / 2, 0 , yu - stepSize / 2);
				vertices[v+1] = new Vector3(xu - stepSize / 2, 0 , yu + stepSize / 2);
				vertices[v+2] = new Vector3(xu + stepSize / 2, 0 , yu - stepSize / 2);

				vertices[v+3] = new Vector3(xu + stepSize / 2, 0 , yu - stepSize / 2);
				vertices[v+4] = new Vector3(xu - stepSize / 2, 0 , yu + stepSize / 2);
				vertices[v+5] = new Vector3(xu + stepSize / 2, 0 , yu + stepSize / 2);
                // uvs
                uvs[v + 0] = new Vector2(0,0);
                uvs[v + 1] = new Vector2(0,1);
                uvs[v + 2] = new Vector2(1,0);

                uvs[v + 3] = new Vector2(1,0);
                uvs[v + 4] = new Vector2(0,1);
                uvs[v + 5] = new Vector2(1,1);
                // vertex colors
                colors [v + 0] = Color.grey;
				colors [v + 1] = Color.grey;
				colors [v + 2] = Color.grey;

				colors [v + 3] = Color.grey;
				colors [v + 4] = Color.grey;
				colors [v + 5] = Color.grey;
                // triangle indices
				triangles[v+0] = v + 0;
				triangles[v+1] = v + 1;
				triangles[v+2] = v + 2;
				triangles[v+3] = v + 3;
				triangles[v+4] = v + 4;
				triangles[v+5] = v + 5;
			}
		}
		mesh.vertices = vertices;
        mesh.uv = uvs;
		mesh.colors = colors;
		mesh.triangles = triangles;
		mesh.RecalculateNormals();
		GetComponent<MeshCollider>().sharedMesh = mesh;
	}



}
