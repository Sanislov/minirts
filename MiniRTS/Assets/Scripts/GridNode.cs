﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class GridNode
{
    public Vector2Int gPos;
    public Vector3 wPos;
    public bool[] roads = new bool[4];
    [System.NonSerialized]
    public GameObject City;

    // Editor variables
    [System.NonSerialized]
    public GameObject[] roadObjects = new GameObject[4];

    // pathfinding variables
    public Vector2Int parentPos;
    public GridNode parent;
    public int gCost;
    public int hCost;
 

/*
    public GridNode(int _x, int _y, float steps)
    {
        roads = new bool[4];
        roadObjects = new GameObject[4];
        wPos = new Vector3(_x * steps, 0.0f, _y * steps);
        gPos = new Vector2Int( _x ,_y);
    }
*/
    public int fCost
    {
        get { return gCost + hCost; }
    }
}
