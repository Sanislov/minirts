﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pathfinding : MonoBehaviour
{
    public Transform GridObject;
    public MapGrid grid;

    void Start()
    {
        grid = GridObject.GetComponent<MapGrid>();
        if (grid.MapData.Cities.Count == 2)
        {
            Debug.Log("start Pathfinding!");
            CityController city0 = grid.MapData.Cities[0].GetComponent<CityController>();
            CityController city1 = grid.MapData.Cities[1].GetComponent<CityController>();
            FindPath(city0.parentGrid, city1.parentGrid);
        }
    }

    void Update()
    {
      
    }

    void FindPath(GridNode startNode, GridNode targetNode)
    {
        List<GridNode> openSet = new List<GridNode>();
        HashSet<GridNode> closedSet = new HashSet<GridNode>();

        openSet.Add(startNode);

        while (openSet.Count > 0)
        {
            GridNode currentNode = openSet[0];
           
            for (int i = 0; i < openSet.Count; i++)
            {
                if (openSet[i].fCost < currentNode.fCost || openSet[i].fCost == currentNode.fCost && openSet[i].hCost < currentNode.hCost)
                {
                    currentNode = openSet[i];
                }
            }

            openSet.Remove(currentNode);
            closedSet.Add(currentNode);

            if (currentNode == targetNode)
                {
                Debug.Log("target found!");
                RetracePath(startNode,targetNode);
                return;    
            }

            foreach (GridNode neighbour in grid.GetNeighbours(currentNode))
            {
                if (closedSet.Contains(neighbour))
                {
                    continue;
                }

                int newMovementCostToNeighbour = currentNode.gCost + GetDistance(currentNode, neighbour);
                if (newMovementCostToNeighbour < neighbour.gCost || !openSet.Contains(neighbour))
                {
                    neighbour.gCost = newMovementCostToNeighbour;
                    neighbour.hCost = GetDistance(neighbour, targetNode);
                    neighbour.parentPos = currentNode.gPos;
                    neighbour.parent = currentNode;

                    if (!openSet.Contains(neighbour))
                    {
                        openSet.Add(neighbour);
                    }
                }
            }
        }
    }

    void RetracePath(GridNode startNode, GridNode endNode)
    {
        List<GridNode> path = new List<GridNode>();
        GridNode currentNode = endNode;
       
        int limiter = 0;

        while (currentNode != startNode && limiter < 10)
        {
            Debug.Log("currentNode: " + currentNode.gPos.ToString());
            Debug.Log("currentNodeparent: " + currentNode.parentPos.ToString());
            path.Add(currentNode);      
            currentNode = grid.GetNodebyPos(currentNode.parentPos);
            limiter++;
        }

        path.Reverse();

        grid.path = path;
    }



    int GetDistance(GridNode nodeA, GridNode nodeB)
    {
        int distX = Mathf.Abs(nodeA.gPos.x - nodeB.gPos.x);
        int distY = Mathf.Abs(nodeA.gPos.y - nodeB.gPos.y);

        if (distX > distY)
            return 14 * distY + 10 * (distX-distY);
        return 14 * distX + 10 * (distY - distX);
    }


	
}
