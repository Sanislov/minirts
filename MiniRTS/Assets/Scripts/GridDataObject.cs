﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/GridDataObject", order = 1)]
public class GridDataObject : ScriptableObject
{
    public float stepSize = 25;
    public int gridX = 2;
    public int gridY = 2;
    public GridNode[] MapArray;

    [System.NonSerialized]
    public List<GameObject> Cities;
}