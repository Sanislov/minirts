﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(MapGrid))]
public class GeneratorButton : Editor
{
    public override void OnInspectorGUI()
    {      
        MapGrid myScript = (MapGrid)target;
        if (GUILayout.Button("Build Object"))
        {
            myScript.BuildMap();
        }

        DrawDefaultInspector();
    }
}