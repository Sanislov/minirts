﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class LevelEditorWindow : EditorWindow
{
    int selectedAction = 0;
    int gridlayermask;
    bool isRoadBuilding = false;
    GridNode lastRoadGrid;
    GridNode actRoadGrid;

    Vector2Int[] griddirs = new Vector2Int[4]
{
        new Vector2Int(0, 1), new Vector2Int(1, 0), new Vector2Int(0, -1), new Vector2Int(-1, 0)
};


    [MenuItem("Window/Level Editor")]
    static void Init()
    {
        LevelEditorWindow window = (LevelEditorWindow)EditorWindow.GetWindow(typeof(LevelEditorWindow));
        window.Show();
    }


    void OnGUI()
    {
		HandleUtility.AddDefaultControl(GUIUtility.GetControlID(FocusType.Passive));

        float width = position.width - 5;
        float height = 60;
	
        string[] actionLabels = new string[] { "City", "Road"};
        selectedAction = GUILayout.SelectionGrid(selectedAction, actionLabels, 2, GUILayout.Width(width), GUILayout.Height(height));

    }



    void OnScene(SceneView sceneview)
    {
        gridlayermask = 1 << LayerMask.NameToLayer("EditorGrid");

        int controlId = GUIUtility.GetControlID(FocusType.Passive);
        Event e = Event.current;
        if (e.type == EventType.MouseDown)
        {
            RaycastHit hit;
            Ray ray = HandleUtility.GUIPointToWorldRay(Event.current.mousePosition);


            // Create City
            if (selectedAction == 0)
            {
                if (Physics.Raycast(ray, out hit, maxDistance: Mathf.Infinity, layerMask: gridlayermask))
                {
                    GUIUtility.hotControl = controlId;
                    Event.current.Use();

                    MapGrid gridscript = hit.transform.GetComponent<MapGrid>();
                    if (e.button == 0)
                    {  
                        int triID = hit.triangleIndex;
                        gridscript.CreateCity(triID);
                    }
                    if (e.button == 1)
                    {
                        int triID = hit.triangleIndex;
                        gridscript.DeleteCity(triID);
                    }
                }
            }

            // Road building
            if (selectedAction == 1)
            {
                if (Physics.Raycast(ray, out hit, maxDistance: Mathf.Infinity, layerMask: gridlayermask))
                {
                    GUIUtility.hotControl = controlId;
                    Event.current.Use();
                    // start road
                    if (e.button == 0 || e.button == 1)
                    {
                        MapGrid gridscript = hit.transform.GetComponent<MapGrid>();
                        int triID = hit.triangleIndex;
                        lastRoadGrid = gridscript.GetNode(triID);
                        isRoadBuilding = true;
                    }
                }
            }

            // Debug grid data
            if (selectedAction == 2)
            {
                if (Physics.Raycast(ray, out hit, maxDistance: Mathf.Infinity, layerMask: gridlayermask))
                {
                    GUIUtility.hotControl = controlId;
                    Event.current.Use();
                    // debuglog
                    if (e.button == 0)
                    {
                        MapGrid gridscript = hit.transform.GetComponent<MapGrid>();
                        int triID = hit.triangleIndex;
                        int x = (triID / 2) / gridscript.gridX;
                        int z = (triID / 2) % gridscript.gridY;
                        Debug.Log("x: " + x.ToString() + " z: " + z.ToString());
                    }
                }
            }
        }
        else if (e.type == EventType.MouseDrag)
        {
            RaycastHit hit;
            Ray ray = HandleUtility.GUIPointToWorldRay(Event.current.mousePosition);
            
            if (selectedAction == 1 && isRoadBuilding)
            {
                if (Physics.Raycast(ray, out hit, maxDistance: Mathf.Infinity, layerMask: gridlayermask))
                {
                    GUIUtility.hotControl = controlId;
                    Event.current.Use();
                    // create road
                    if (e.button == 0)
                    {
                        MapGrid gridscript = hit.transform.GetComponent<MapGrid>();
                        int triID = hit.triangleIndex;
                        actRoadGrid = gridscript.GetNode(triID);

                        int roadsideL = 0;
                        int roadsideA = 0;
                        if (actRoadGrid != lastRoadGrid)
                        {
                            for (int i = 0; i < 4; i++)
                            {
                                int xNpos = lastRoadGrid.gPos.x + griddirs[i].x;
                                int yNpos = lastRoadGrid.gPos.y + griddirs[i].y;
                                if (xNpos >= 0 && xNpos < gridscript.gridX && yNpos >=0 && yNpos < gridscript.gridY)
                                { 
                                    GridNode Ngrid = gridscript.MapData.MapArray[xNpos * gridscript.gridX + yNpos];
                                    if (Ngrid == actRoadGrid)
                                    {
                                        lastRoadGrid.roads[i] = true;
                                        roadsideL = i;
                                        if (i < 2)
                                        { actRoadGrid.roads[i + 2] = true;
                                            roadsideA = i + 2;
                                        }
                                        else
                                        { actRoadGrid.roads[i - 2] = true;
                                            roadsideA = i - 2;
                                        }
                                    }
                                }                          
                            }
                            gridscript.CreateRoad(lastRoadGrid, actRoadGrid, roadsideL, roadsideA);
                            lastRoadGrid = actRoadGrid;
                        }                  
                    }
                    // delete road
                    if (e.button == 1)
                    {
                        MapGrid gridscript = hit.transform.GetComponent<MapGrid>();
                        int triID = hit.triangleIndex;
                        actRoadGrid = gridscript.GetNode(triID);

                        int roadsideL = 0;
                        int roadsideA = 0;
                        if (actRoadGrid != lastRoadGrid)
                        {
                            for (int i = 0; i < 4; i++)
                            {
                                int xNpos = lastRoadGrid.gPos.x + griddirs[i].x;
                                int yNpos = lastRoadGrid.gPos.y + griddirs[i].y;
                                if (xNpos >= 0 && xNpos < gridscript.gridX && yNpos >= 0 && yNpos < gridscript.gridY)
                                {
                                    GridNode Ngrid = gridscript.MapData.MapArray[xNpos * gridscript.gridX + yNpos];
                                    if (Ngrid == actRoadGrid)
                                    {
                                        lastRoadGrid.roads[i] = false;
                                        roadsideL = i;
                                        if (i < 2)
                                        {
                                            actRoadGrid.roads[i + 2] = false;
                                            roadsideA = i + 2;
                                        }
                                        else
                                        {
                                            actRoadGrid.roads[i - 2] = false;
                                            roadsideA = i - 2;
                                        }
                                    }
                                }
                            }
                            gridscript.DeleteRoad(lastRoadGrid, actRoadGrid, roadsideL, roadsideA);
                            lastRoadGrid = actRoadGrid;
                        }
                    }
                }
            }

        }

    }

    void OnEnable()
    {
        SceneView.onSceneGUIDelegate -= OnScene;
        SceneView.onSceneGUIDelegate += OnScene;
    }

	void OnDestroy()
	{
		SceneView.onSceneGUIDelegate -= OnScene;
	}

}
